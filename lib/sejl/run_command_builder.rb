module Sejl

  class RunCommandBuilder

    attr_reader :root, :name

    def initialize(name, root_path, &block)
      @name = name
      @root = Pathname.new(root_path)
      raise ArgumentError unless @root.exist? && @root.directory?
      @bools = []
      instance_eval(&block)
    end

    def to_cmd
      cmd = <<-SHELL.gsub(/^\s+/, '').gsub(/\n/,' ')
        cd #{root} &&
        sudo docker run
        #{@bools.join(' ')}
        #{volume_mappings}
        #{network_mapping}
        -w #{@workdir}
        #{volumes_from_options}
        #{@image}
        #{@command}
      SHELL
    end

    def execute
      puts "Executing: #{to_cmd}"
      puts `#{to_cmd}`
    end

    def interactive
      @bools += %w{-i -t}
    end

    def daemonized
      @bools << '-d'
    end

    def workdir(dir)
      @workdir = dir
    end

    # def cidfile(path)
    #   @cidfile = path
    # end

    def cidfile
      @root.join(@name+'.cid')
    end

    def volumes(mapping)
      @volumes = mapping
    end

    def volume_mappings
      @volumes.map{ |pair| '-v '+pair.join(':') }.join(' ') if @volumes
    end

    def volume_from(*container_names)
      @volumes_from = container_names
    end

    def volumes_from_options
      @volumes_from.map{ |name| "-volume-from #{name}"}.join(' ') if @volumes_from
    end

    def network(mapping)
      @network = mapping
    end

    def network_mapping
      @network.to_a.map{ |m| '-p ' + m.join(':') }.join(' ') if @network
    end

    def image(image)
      @image = image
    end

    def command(cmd)
      @command = cmd
    end

  end

end
