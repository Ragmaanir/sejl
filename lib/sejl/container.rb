module Sejl

  class Container

    attr_reader :name

    def initialize(name)
      @name = name
    end

    def build
      PTY.spawn("sudo docker build -t #{name} #{ROOT}/#{name}") do |stdin, stdout, pid|
        spinner = Torden::Animation.new
        @log = []

        begin
          stdin.each do |line|
            spinner.update

            @log << line

            str = "Building %s [#{spinner}] \r" % [name, line.gsub(/[\r\n]|\e\[2K/,'')]
            print str
          end
        rescue Errno::EIO
        end

        puts

        if m = @log.reverse.take(20).join(';').match(/successfully built ([0-9a-z]+)/i)
          # `sudo docker tag #{m[1]} #{name}`
          puts Rainbow(@log.last).color(:green)
        else
          puts @log.join("\n")
          puts Rainbow("ERROR: #{@log.last}").color(:red)
        end
      end
    end
  end

end
