require 'pty'

module Sejl

  class ContainerDefinition

    def initialize
    end

    attr_reader :configurations

    def build
    end
  end

  class ContainerDefinitionBuilder
  end

  class Configuration
    def initialize(dependencies)
      @dependencies = dependencies
    end

    attr_reader :dependencies

    def name
      raise
    end

    def check_prerequesites
    end

    def apply_dependencies(context)
      dependencies.each do |dep|
        dep.apply(context)
      end
    end

    def apply(context)
      apply_dependencies(context)
    end
  end

  class Shell
    def initialize
      @master, @slave = PTY.open
      @read, @write = IO.pipe
      @pid = spawn('/bin/bash', in: @read, out: @slave)
    end

    def execute(cmd)
      @write.puts(cmd)
      p @master.gets
    end
  end

  class VagrantShell

  end

  class ContainerContext
    def initialize(image)
      @stdin, @stdout = PTY.spawn("sudo docker run -i -t #{image} /bin/bash")
    end

    attr_reader :command_history

    def execute(cmd)

      push_command(cmd) do |log|
        begin
          @stdout.puts cmd
          @stdin.each do |line|
            log << line
          end
        rescue Errno::EIO
        end
      end

    end

    def execute_lines(cmds)
      #cmds.split("\n").gsub(/\A\s*/,'')
      cmds = cmds.split("\n").map(&:strip).reject(&:blank?)
      cmds.each do |cmd|
        execute(cmd)
      end
    end

    def commit(message: "")

    end

  private

    def push_command(cmd, &block)
      @command_history << cmd
      @output_history[@command_history.length] = []
      block.call(@output_history[@command_history.length])
    end

    # def log_output(output)
    #   @output_history[@command_history.length] << output
    # end
  end

  class AptPackageConfiguration < Configuration
    def initialize(packages)
      super([])
    end

    def name
      "apt-get: #{package_string}"
    end

    def apply(context)
      context.execute("apt-get install -y #{package_string}")
      commit(message: "Installed packages: #{package_string}")
    end

  private

    def package_string
      packages.join(' ')
    end
  end

  class ScriptConfiguration < Configuration
    def self.define(**options, &block)
      new(options.merge(script: block))
    end

    def initialize(script:, params:, **options)
      @script = script
      @params = params
    end

    attr_reader :script, :params

    def apply_script(context)
      @script.call(context, OpenStruct.new(params))
    end

    def apply(context)
      super
      apply_script(context)
    end
  end

  Git = AptPackageConfiguration.new('git')
  Base = AptPackageConfiguration.new(%w{htop iotop curl wget screen tmux nano ssh})
  RubyDeps = AptPackageConfiguration.new(%w{libc6-dev libssl-dev libmysql++-dev libsqlite3-dev make build-essential libssl-dev libreadline6-dev zlib1g-dev libyaml-dev libncurses-dev})
  JRE = AptPackageConfiguration.new('openjdk-7-jre-headless')

  RBEnvConfiguration = ScriptConfiguration.define(dependencies: [Base, Git, RubyDeps], params: {repo: "https://github.com/sstephenson", home: "/home/main"}) do |context, params|
    context.execute_lines <<-SHELL
      git clone #{params.repo}/rbenv.git #{params.home}/.rbenv
      git clone #{params.repo}/ruby-build.git #{params.home}/.rbenv/plugins/ruby-build

      echo 'export PATH="#{params.home}/.rbenv/bin:$PATH"' >> #{params.home}/.bashrc
      echo 'eval "$(rbenv init -)"' >> #{params.home}/.bashrc

      export PATH="#{params.home}/.rbenv/bin:$PATH"
      eval "$(rbenv init -)"

      #{params.home}/.rbenv/plugins/ruby-build/install.sh
    SHELL
  end

  RubyConfiguration = ScriptConfiguration.define(dependencies: [RBEnvConfiguration], params: {version: "2.1.1"}) do |context, params|
    patch = "curl -fsSL https://gist.github.com/mislav/a18b9d7f0dc5b9efc162.txt | rbenv install --patch 2.1.1" if params.version == "2.1.1"

    context.execute_lines <<-SHELL
      # https://github.com/sstephenson/ruby-build/issues/526
      #{patch}
      rbenv global #{params.version}
      echo 'gem: --no-rdoc --no-ri' >> ~/.gemrc

      # http://stackoverflow.com/questions/21095098/why-wont-bundler-install-json-gem
      gem install bundler -v '= 1.5.1'
      gem install json -v '1.8.1'
      rbenv rehash
    SHELL
  end

  ElasticSearchConfig = ScriptConfiguration.define(dependencies: [JRE], params: {version: '1.1.0'}) do |context, params|
    context.execute_lines <<-SHELL
      wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-#{params.version}.tar.gz
      mkdir applications
      tar -xvf elasticsearch-#{params.version}.tar.gz
      rm elasticsearch-#{params.version}.tar.gz
      mv elasticsearch-#{params.version} applications/elasticsearch
      #ln -s /home/main/applications/elasticsearch/bin/elasticsearch /home/main/bin
    SHELL
  end

end
