module Sejl

  class RunCommand
    def initialize( name:, root_path:, image:,
                    volume: nil, network: nil,
                    interactive: true, detached: true)
      @name = name
      @root = Pathname.new(root_path)
      raise ArgumentError unless @root.exist? && @root.directory?
      @image = image
      @volume, @network = volume, network
      @interactive, @detached = interactive, detached
    end

    def volume_option
      "-v #{@volume}" if @volume
    end

    def network_option
      "-p #{@network}" if @network
    end

    def bool_options
      opts = []
      opts << '-i -t' if @interactive
      opts << '-d' if @detached
      opts.join(' ')
    end

    def to_cmd
      cmd = <<-SHELL.gsub(/^\s+/, '').gsub(/\n/,' ')
        cd #{@root} &&
        sudo docker run
        #{bool_options}

        #{volume_option}
        #{network_option}
        -w /home/main
        --name #{@name}
        #{@image}
        /home/main/shared/run.sh
      SHELL
    end

    def execute
      print "#{to_cmd}: "
      puts `#{to_cmd}`
    end
  end

end
