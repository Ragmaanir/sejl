describe Sejl::RunCommandBuilder do
  it '' do
    described_class.new('thename', ".") do
      interactive
      daemonized
      volumes "a" => "a", "x" => "y"
    end.to_cmd.should == "cd . && sudo docker run -i -t -d -v a:a -v x:y -w /home/main "
  end
end
