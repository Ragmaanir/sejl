require 'sejl'
require 'pty'

# This file was generated by the `rspec --init` command. Conventionally, all
# specs live under a `spec` directory, which RSpec adds to the `$LOAD_PATH`.
# Require this file using `require "spec_helper"` to ensure that it is only
# loaded once.
#
# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.run_all_when_everything_filtered = true
  config.filter_run :focus

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'

  MARKER = '--- VAGRANT START ---'

  config.before(:all, :integration) do
    @stdin, @vagrant = PTY.spawn("vagrant ssh")
    #@vagrant.puts 'eval "$(rbenv init -)"'
    execute 'cd /vagrant'
    @vagrant.puts "echo #{MARKER}"
    while !@stdin.gets.start_with?(MARKER)
    end
  end

  config.after(:all, :integration) do
    begin
      @vagrant.puts 'exit'

      lines = []
      @stdin.each do |line|
        lines << line.gsub(/(\e\[\d\d;\d\dm)|(\e\]0;)|(\e\[0m)/,'')
      end
    rescue Errno::EIO
    ensure
      #lines = lines.drop_while{ |line| !line.start_with?(MARKER) }.drop(1)
      puts lines
    end
  end

  def execute(cmd)
    @vagrant.puts cmd
    #p @stdin.gets
  end

end
